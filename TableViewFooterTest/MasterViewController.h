//
//  MasterViewController.h
//  TableViewFooterTest
//
//  Created by Zoltan Ulrich on 4/25/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (retain, nonatomic) IBOutlet UILabel *footerLabel;


@end
